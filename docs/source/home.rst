Home
====
Hi, ! Welcome to DiZign!

About Us
""""""""

We are a computer service company that design/integrate/distribute mobile game apps on various platforms including iOS and Android.

We partner with various third party companies to deliver the most engaging experience.



Online Free Trial
"""""""""""""""""

`Play PONG Game Now! <http://www.dizign.org/login?next=%2Fplay_pong_game>`_

`Play FPS Game Now! <http://www.dizign.org/login?next=%2Fplay_fps_game>`_



Articles About Our Games
""""""""""""""""""""""""

`Top Android Games in December 2021 <https://gameskeys.net/top-android-games-to-tryout-in-december-2021/>`_



Contact Us
""""""""""
Email: contact@dizign.org

Phone: 650 382 2390
Privacy Policy
==============

This privacy policy explains how collect and process your personal data when you access and play our applications and services.

1. Consent to Policy
""""""""""""""""""""

By installing and using the application, you consent to this Privacy Policy and the collection, use and sharing of the personal data described in this Policy. If you do not agree, do not install the application or use the application.

2. Scope of Policy
""""""""""""""""""

The scope of this Privacy Policy is limited to the personal data collected by you using our applications and services.

3. Information and Data we collect from you
"""""""""""""""""""""""""""""""""""""""""""

When you install and use Dizign LLC mobile applications, we can collect and process some of your data for different legitimate purposes.

You will find below explanations regarding the reasons why we may collect data.

**Why is data collected?**

DiZign LLC and its third-party partners collect data:

To provide you with the services you asked for To run analytics and understand how users interact with our product and services to improve them continuously The third-party partners that DiZign LLC will share the data with are:

GameAnalytics, Facebook Analytics, Adjust, Voodoo

**Data deletion requests**

To delete user data collected by DiZign LLC, please reach out to contact@dizign.org

If you also want to request the deletion of the data that may have been collected about you or your device by our third-party partners that are acting as independent data controllers, you will find below the contact for each partner and the link to the relevant section of their privacy policies:

| GameAnalytics
| email: privacy@gameanalytics.com
| privacy policy: https://gameanalytics.com/privacy/#6-changing-or-deleting-your-information
|
| Facebook Analytics
| email: none
| privacy policy: https://www.facebook.com/policy.php
|
| Adjust
| email: privacy@adjust.com
| privacy policy: https://www.adjust.com/terms/privacy-policy
|
| Voodoo
| Voodoo does not collect any of your personal data and only provides an SDK package to retrieve the data collected by the above-mentioned partners. You can still contact them to request the deletion of their access to the partners’ interfaces.
| email: dpo@voodoo.io

4. Rights of users under GDPR or the equivalents
""""""""""""""""""""""""""""""""""""""""""""""""

If you are under GDPR or the equivalents, you have the following rights regarding the processing of personal data.
When you exercise the following rights, you can send a request to the contacts listed in the "Contacts" section.
We may ask you to provide certain information in order to identify and correct your data.

**Right to withdraw consent**

You have the right to withdraw your consent at any time in accordance with Article 7(3), paragraph 3 of the GDPR.

If you wish to withdraw your consent, please contact us at the address specified in this policy to withdraw your consent. The withdrawal of this consent cannot affect the legality of the treatment based on the consent before the withdrawal.

If you withdraw your consent, you will not be able to continue using the applications and services.

**Right of access and rectification**

You have the right to receive confirmation as to whether or not personal data concerning him or her are being processed, and, where that is the case, access to the personal data in accordance with Article 15 of the GDPR.

You also have the right to obtain from us without undue delay the rectification of inaccurate personal data concerning you in accordance with Article 16 of the GDPR.

**Right of erasure and restriction of processing**

In accordance with Article 17 of the GDPR, you shall have the right to obtain from us the erasure of personal data concerning you without undue delay.

You also have the right to restrict the handling of personal data in accordance with Article 18 of the GDPR.

However, if your personal data is legitimately used to provide our services and for other purposes, we may reject your request or ask you to postpone the response.

**Right to data portability**

In accordance with Article 20 of the GDPR, you shall have the right to receive the personal data which you have provided to us, in a structured, commonly used and machine-readable format and have the right to transmit those data to another controller without hindrance from us to which the personal data have been provided.

Additionally, you have the right to have the personal data transmitted directly from us to another, where technically feasible.

**Right to object**

In accordance with Article 21 of the GDPR, you shall have the right to object, on grounds relating to your particular situation, at any time to processing of personal data concerning you.

**Right to lodge a complaint with a supervisory authority**

In accordance with Article 77 of the GDPR, without prejudice to any other administrative or judicial remedy, you shall have the right to lodge a complaint with a supervisory authority, in particular in the Member State of his or her habitual residence, place of work or place of the alleged infringment if you considers that the processing of personal data relating to you infringes this Regulation.

**Transferring of your personal data**

We may transfer and process your personal data from your country or region of residence to Japan and other countries around the world. We may share your information with international organizations as partner. Please note that these countries and jurisdictions may not have the same data protection laws as your country.

5. Contact
""""""""""

Email: contact@dizign.org

6. Change of Privacy Policy
"""""""""""""""""""""""""""

We may update this Privacy Policy from time to time as necessary.